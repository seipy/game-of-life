const ROW_LENGTH = 24;
const COL_LENGTH = 24;
const FRAMERATE = 60;
const SPEED_INCREMENT = 5;

let board;
let isGameRunning;
let gameSpeed;
let timeTillNextRefresh;

function create() {
    board = Array(COL_LENGTH).fill(null).map(() => Array(ROW_LENGTH).fill(false));
    isGameRunning = false;
    gameSpeed = 30;
    timeTillNextRefresh = 0;
}

function getNeighbourCount(col, row) {
    function getNeighbour(c, r) {
        if (c < 0 || c >= COL_LENGTH || r < 0 || r >= ROW_LENGTH) return false;
        return board[c][r];
    }

    return [[-1, -1], [-1, 0], [-1, 1], [0, -1], [0, 1], [1, -1], [1, 0], [1, 1]]
        .map(([c, r]) => getNeighbour(col + c, row + r))
        .filter((isAlive) => isAlive)
        .length;
}

function update(game) {
    if (isGameRunning) {
        if (timeTillNextRefresh <= 0) {
            const updatedBoard = Array(COL_LENGTH).fill(null).map(() => Array(ROW_LENGTH).fill(false));

            board.forEach((col, colIndex) => {
                col.forEach((isAlive, rowIndex) => {
                    const neighbourCount = getNeighbourCount(colIndex, rowIndex);
                    
                    /*
                    Any live cell with two or three live neighbours survives.
                    Any dead cell with three live neighbours becomes a live cell.
                    All other live cells die in the next generation. Similarly, all other dead cells stay dead.
                    */
                   const survives = isAlive && (neighbourCount === 2 || neighbourCount === 3);
                   const revived = !isAlive && neighbourCount === 3;

                    updatedBoard[colIndex][rowIndex] =  survives || revived;
                });
            });

            board = updatedBoard;
            timeTillNextRefresh = gameSpeed;
        }
        timeTillNextRefresh -= 1;
    }

    board.forEach((col, colIndex) => {
        col.forEach((isAlive, rowIndex) => {
            game.setDot(colIndex, rowIndex, isAlive ? Color.Indigo : Color.Gray)
        });
    });
}

function onKeyPress(direction) {
    if (direction === Direction.Right) isGameRunning = true;
    else if (direction === Direction.Left) isGameRunning = false;

    else if (direction === Direction.Up) {
        if (gameSpeed > SPEED_INCREMENT) {
            gameSpeed -= SPEED_INCREMENT;
            timeTillNextRefresh -= gameSpeed;
        }
    }

    else if (direction === Direction.Down) {
        gameSpeed += SPEED_INCREMENT;
        timeTillNextRefresh += SPEED_INCREMENT;
    }
}

function onDotClicked(col, row) {
    board[col][row] = !board[col][row];
}

const game = new Game({
    create,
    update,
    onKeyPress,
    onDotClicked,
    frameRate: FRAMERATE,
});

game.run();